from class_path.Flight import Flight
from class_path.Passenger import Passenger


f = Flight('Goa', 'Mumbai', 90)
f2 = Flight('Delhi', 'Mumbai', 80)

f.delay(10)

alice = Passenger('Alice')
bob = Passenger('Bob')
umar = Passenger('Umar')

f.add_passenger(alice)
f.add_passenger(bob)
f2.add_passenger(umar)

# Print detail of flight
f.print_info()
f2.print_info()
