class Flight:
    counter = 1

    def __init__(self, origin, destination, duration):
        self.id = Flight.counter
        Flight.counter += 1

        # Kepp track of passenger
        self.passengers = []

        self.origin = origin
        self.destination = destination
        self.duration = duration

    def print_info(self):
        print(f"Flight from {self.origin} to {self.destination} in {self.duration} minutes.")
        print()
        print('Passengers:')
        for passenger in self.passengers:
            print(f" {passenger}")
        print()

    def delay(self, time_in_minute):
        self.duration += time_in_minute

    def add_passenger(self, p):
        self.passengers.append(p.name)
        p.flight_id = self.id