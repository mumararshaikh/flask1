from lib.config import create_config_file
import datetime
from flask import Flask, render_template

app = Flask(__name__)

# Generate the configuration files
config = create_config_file('config/app.conf')


@app.route("/")
def index():
    now = datetime.datetime.now()
    new_year = (now.month == 1) and (now.day == 1)
    return render_template('index.html', new_year=new_year, config = config)


@app.route("/next")
def next_url():
    return render_template('next.html')


@app.route("/hello")
def hello():
    return render_template('hello.html')

