# This is a flight class.

class Flight:
    counter = 1

    def __init__(self, origin, destination, duration):
        self.id = Flight.counter
        Flight.counter += 1

        # Kepp track of passenger
        self.passengers = []

        self.origin = origin
        self.destination = destination
        self.duration = duration

    def print_info(self):
        print(f"Flight from {self.origin} to {self.destination} in {self.duration} minutes.")
        print()
        print('Passengers:')
        for passenger in self.passengers:
            print(f" {passenger}")
        print()

    def delay(self, time_in_minute):
        self.duration += time_in_minute

    def add_passenger(self, p):
        self.passengers.append(p.name)
        p.flight_id = self.id


class Passenger:
    def __init__(self, name):
        self.name = name


def main():
    f = Flight('Goa', 'Mumbai', 90)
    f2 = Flight('Delhi', 'Mumbai', 80)

    f.delay(10)

    alice = Passenger('Alice')
    bob = Passenger('Bob')
    umar = Passenger('Umar')

    f.add_passenger(alice)
    f.add_passenger(bob)
    f2.add_passenger(umar)

    # Print detail of flight
    f.print_info()
    f2.print_info()


if __name__ == '__main__':
    main()
