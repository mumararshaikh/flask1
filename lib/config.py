def read_config_file(name_of_file):
    """

    :rtype: list
    """
    file_content = open(name_of_file, "r")
    if file_content.mode == "r":
        reader = file_content.readlines()
    else:
        reader = False
    file_content.close()
    return reader


def check_list(list_to_check):
    if len(list_to_check) == 2:
        return True
    else:
        return False


def create_config_file(name_of_file):
    """

    :rtype: dict
    :type name_of_file: str
    """
    config = {}

    all_line = read_config_file(name_of_file)

    for each_line in all_line:
        if each_line[0] != '#' and len(each_line) != 1:
            temp_list = each_line.split()
            if check_list(temp_list):
                config[temp_list[0]] = temp_list[1]
    return config


def main():
    my_dict = create_config_file("app.conf")
    print(my_dict)


if __name__ == "__main__":
    main()
